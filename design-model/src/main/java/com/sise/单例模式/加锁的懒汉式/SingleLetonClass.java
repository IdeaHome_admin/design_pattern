package com.sise.单例模式.加锁的懒汉式;

/**
 * 作者：idea
 * 日期：2018/2/14
 * 描述：加了锁之后的懒汉式
 */
//优点：线程安全 延迟创建
//缺点：容易出现排队堵塞，同步下效率低
public class SingleLetonClass {

    //唯一的一个私有属性，通过外界的函数来调用
    private static SingleLetonClass one;

    private SingleLetonClass(){}

    public synchronized static SingleLetonClass getOne(){
        if(one==null){
            one=new SingleLetonClass();
        }
        return one;
    }
}
