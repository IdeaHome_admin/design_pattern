package com.sise.单例模式.懒汉式;

/**
 * 作者：idea
 * 日期：2018/2/14
 * 描述：懒汉式单例模式(不加锁的模式)
 */
//缺点：
//线程不安全
//延迟创建
public class SingleLetonClass {
    private static SingleLetonClass one;

    private SingleLetonClass(){}

    public static SingleLetonClass getOne(){
        if(one==null){
            return one;
        }else{
            return one;
        }
    }
}
