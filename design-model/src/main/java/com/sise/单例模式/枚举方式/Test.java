package com.sise.单例模式.枚举方式;

/**
 * 作者：idea
 * 日期：2018/2/14
 * 描述：使用枚举方式来调用单例
 */
public class Test {
    public static void main(String[] args) {
        Resource re=new Resource();
        SingletonClass s1=SingletonClass.INSTANCE;
        re=s1.getInstance();
    }
}
