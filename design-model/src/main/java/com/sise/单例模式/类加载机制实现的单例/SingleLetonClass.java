package com.sise.单例模式.类加载机制实现的单例;

/**
 * 作者：idea
 * 日期：2018/2/14
 * 描述：使用类加载机制实现的单例
 */
public class SingleLetonClass {
    private  SingleLetonClass(){}

    //静态内部类，用于持有唯一的SingleLetonClass
    private static class OnlyInstanceHolder{
        static private SingleLetonClass ONLY=new SingleLetonClass();
    }

    public static SingleLetonClass getInstance(){
        return OnlyInstanceHolder.ONLY;
    }

}
