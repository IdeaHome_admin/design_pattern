package com.sise.原型模式;

/**
 * 作者：idea
 * 日期：2018/5/1
 * 描述：原型模式一般用于将一些抽象的内容定义好
 */
public class Dog implements Cloneable{
    public int leg;

    @Override
    public String toString() {
        return "这条狗有" + leg +"条腿";
    }


    public Dog(){
    }

    public Dog(int leg){
        this.leg=leg;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
