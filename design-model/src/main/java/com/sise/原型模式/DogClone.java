package com.sise.原型模式;

/**
 * 作者：idea
 * 日期：2018/5/1
 * 描述：
 */
public class DogClone implements Cloneable{

    public Dog dog=new Dog(4);
    public int legCount; //腿的条数


    public Object clone() throws CloneNotSupportedException {
        DogClone obj=null;
        try {
            obj=(DogClone)super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        obj.dog= (Dog) dog.clone();

        return obj; //克隆对应的这个类
    }

}
