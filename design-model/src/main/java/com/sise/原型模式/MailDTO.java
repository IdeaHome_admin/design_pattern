package com.sise.原型模式;

import lombok.Data;

/**
 * @Author idea
 * @Date: Created in 22:10 2023/10/5
 * @Description
 */
@Data
public class MailDTO implements Cloneable{

    private String title;
    private String address;
    private String content;


    @Override
    public MailDTO clone() {
        try {
            //需要继承了clone接口才可以做克隆
            MailDTO clone = (MailDTO) super.clone();
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }
}
