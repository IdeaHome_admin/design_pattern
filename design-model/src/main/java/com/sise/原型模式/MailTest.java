package com.sise.原型模式;

/**
 * @Author idea
 * @Date: Created in 22:10 2023/10/5
 * @Description
 */
public class MailTest {

    public static void main(String[] args) {
        MailDTO mailDTO = new MailDTO();
        mailDTO.setTitle("邮件地址");
        for (int i = 0; i < 10; i++) {
            MailDTO copyItem = mailDTO.clone();
            copyItem.setContent("this is " + i);
            copyItem.setAddress("address");
            System.out.println(copyItem);
        }
    }
}
