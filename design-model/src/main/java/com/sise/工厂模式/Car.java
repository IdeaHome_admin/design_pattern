package com.sise.工厂模式;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：idea
 * 日期：2018/3/23
 * 描述：
 */
public class Car implements Vehicle{
    public static  Car car=new Car();
    public List<Car> carList=new ArrayList<Car>(); //链接池有点类似于如此，使用的是多例模式

    public Car(){}

    //单例模式里面的饿汉式（这个可以叫做静态工厂方法）
    public static Car getInstance(){
        return car;
    }


    public void run(){
        System.out.println("在疯狂地奔跑着...");
    }

}
