package com.sise.工厂模式;

/**
 * 作者：idea
 * 日期：2018/3/23
 * 描述：
 */
public class CarFactory implements VehicleFactory{

    @Override
    public Vehicle create() {
        System.out.println("创建car");
        return (Vehicle) new Car();
    }
}
