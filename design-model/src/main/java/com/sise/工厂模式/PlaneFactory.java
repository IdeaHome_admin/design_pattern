package com.sise.工厂模式;

/**
 * 作者：idea
 * 日期：2018/3/23
 * 描述：
 */
public class PlaneFactory implements  VehicleFactory {

    public Vehicle vehicle;

    @Override
    public Vehicle create() {
        System.out.println("创建飞机");
        vehicle=new Plane();
        return vehicle;
    }
}
