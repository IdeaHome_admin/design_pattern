package com.sise.工厂模式;

/**
 * 作者：idea
 * 日期：2018/3/23
 * 描述：
 */
public class Test {
    public static void main(String[] args) {
        VehicleFactory v1=new PlaneFactory();
        VehicleFactory v2=new CarFactory();
        v1.create();
        v2.create();
    }
}
