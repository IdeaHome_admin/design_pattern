package com.sise.工厂模式;

/**
 * 作者：idea
 * 日期：2018/3/23
 * 描述：
 */
public interface VehicleFactory {
    Vehicle create();
}
