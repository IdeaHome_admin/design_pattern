package com.sise.工厂模式.工厂方法;

import com.sise.工厂模式.工厂方法.session.Session;

/**
 * @Author linhao
 * @Date created in 11:31 上午 2021/5/23
 */
public interface ISessionFactory {

    Session getSession();
}
