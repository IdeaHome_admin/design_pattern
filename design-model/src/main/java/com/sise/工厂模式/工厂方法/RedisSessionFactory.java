package com.sise.工厂模式.工厂方法;


import com.sise.工厂模式.工厂方法.session.RedisSession;
import com.sise.工厂模式.工厂方法.session.Session;

/**
 * @Author linhao
 * @Date created in 11:32 上午 2021/5/23
 */
public class RedisSessionFactory implements ISessionFactory {

    @Override
    public Session getSession() {
        return new RedisSession("redis");
    }
}
