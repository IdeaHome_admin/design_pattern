package com.sise.工厂模式.工厂方法;

import com.sise.工厂模式.工厂方法.session.Session;

/**
 * @Author linhao
 * @Date created in 11:36 上午 2021/5/23
 */
public class Test {

    public static void main(String[] args) {
        ISessionFactory sessionFactory = new RedisSessionFactory();
        Session session = sessionFactory.getSession();
        System.out.println(session.toString());
    }
}
