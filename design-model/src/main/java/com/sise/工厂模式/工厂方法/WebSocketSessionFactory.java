package com.sise.工厂模式.工厂方法;


import com.sise.工厂模式.工厂方法.session.Session;
import com.sise.工厂模式.工厂方法.session.WebSocketSession;

/**
 * @Author linhao
 * @Date created in 11:35 上午 2021/5/23
 */
public class WebSocketSessionFactory implements ISessionFactory {

    @Override
    public Session getSession() {
        return new WebSocketSession("websocket");
    }
}
