package com.sise.工厂模式.工厂方法.session;

/**
 * @Author linhao
 * @Date created in 11:33 上午 2021/5/23
 */
public class Session {

    String name;

    public Session(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Session{" +
                "name='" + name + '\'' +
                '}';
    }
}
