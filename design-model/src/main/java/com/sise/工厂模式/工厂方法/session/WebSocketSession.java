package com.sise.工厂模式.工厂方法.session;

/**
 * @Author linhao
 * @Date created in 11:34 上午 2021/5/23
 */
public class WebSocketSession extends Session {

    public WebSocketSession(String name) {
        super(name);
    }
}
