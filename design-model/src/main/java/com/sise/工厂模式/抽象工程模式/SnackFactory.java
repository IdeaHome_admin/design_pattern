package com.sise.工厂模式.抽象工程模式;

public interface SnackFactory {
    public Snack createSnack();
}
