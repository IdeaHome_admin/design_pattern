package com.sise.工厂模式.抽象工程模式;

public class test {
    public static void main(String[] args) {
        SnackFactory eatSnackFactory=new EatSnackFactory();
        SnackFactory drinkFactory=new DrinkSnackFactory();
        EatSnack es= (EatSnack) eatSnackFactory.createSnack();
        System.out.println(es.toString());
        DrinkSnack ds= (DrinkSnack) drinkFactory.createSnack();
        System.out.println(ds.toString());
    }
}
