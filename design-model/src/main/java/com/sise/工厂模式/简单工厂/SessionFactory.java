package com.sise.工厂模式.简单工厂;


/**
 * @Author linhao
 * @Date created in 11:26 下午 2021/5/22
 */
public class SessionFactory {

    public Session getSession(){
        return new Session();
    }

    public RedisSession getRedisSession(){
        return new RedisSession();
    }

    public ZookeeperSession getZookeeperSession(){
        return new ZookeeperSession();
    }

    public WebSocketSession getWebSocketSession(){
        return new WebSocketSession();
    }

}
