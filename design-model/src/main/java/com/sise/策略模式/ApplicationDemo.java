package com.sise.策略模式;

import com.sise.策略模式.present.IMarketingCoreService;
import com.sise.策略模式.present.impl.MarketingCoreService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


/**
 * @Author idea
 * @Date created in 10:14 上午 2020/5/4
 */
public class ApplicationDemo {

    public static void main(String[] args) throws Exception {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
        applicationContext.scan("com.sise.策略模式.present");
        //启动上下文
        applicationContext.refresh();
        IMarketingCoreService marketingCoreService = applicationContext.getBean(MarketingCoreService.class);
        marketingCoreService.doMarketingJob("p111");
    }
}
