package com.sise.策略模式;


//使用策略枚举
public enum  Calculator {

    ADD("+"){
        public int exec(int a,int b){
            return a+b;
        }
    },
    SUB("-"){
        public int exec(int a,int b){
            return a-b;
        }
    },
    DIV("/"){
        public int exec(int a,int b){
            return a/b;
        }
    },
    MULT("*"){
        public int exec(int a,int b){
            return a*b;
        }
    };

    String value="";
    private Calculator(String value){
        this.value=value;
    }
    public abstract int exec(int a,int b);

}
