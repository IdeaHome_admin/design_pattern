package com.sise.策略模式;

public class CalculatorOriginal {

    public int add(int a,int b){
        return a+b;
    }
    public int div(int a,int b){
        return a/b;
    }
    public int sub(int a,int b){
        return a-b;
    }
    public int muit(int a,int b){
        return a*b;
    }

    public  int exec(int a,int b,String sign){
        if(sign.equals("-")){
            return this.sub(a,b);
        }else if(sign.equals("+")){
            return this.add(a,b);
        }else if(sign.equals("*")){
            return this.muit(a,b);
        }else{
            return this.div(a,b);
        }
    }
}
