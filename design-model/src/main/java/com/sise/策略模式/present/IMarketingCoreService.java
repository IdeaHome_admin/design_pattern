package com.sise.策略模式.present;


/**
 * 营销工具核心执行器
 *
 * @Author idea
 * @Date created in 9:34 上午 2020/5/4
 */
public interface IMarketingCoreService {

    /**
     * 执行不同的营销工具
     *
     * @param productNo 产品编码
     * @return
     */
    boolean doMarketingJob(String productNo) throws Exception;
}
