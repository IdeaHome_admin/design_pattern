package com.sise.策略模式.present;

/**
 * 关于营销手段的策略
 *
 * @Author idea
 * @Date created in 9:20 上午 2020/5/4
 */
public interface IMarketingStrategy {

    /**
     * 服务赠送的策略执行
     *
     * @param param 参数
     * @return
     */
    boolean doMarketing(Object ...param);
}
