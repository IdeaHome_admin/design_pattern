package com.sise.策略模式.present.dao;

import com.sise.策略模式.present.model.MarketingPO;

import java.util.List;

/**
 * @Author idea
 * @Date created in 9:46 上午 2020/5/4
 */
public interface IMarketingDao {

    /**
     * 根据营销id查询
     *
     * @param idList marketingId的集合
     * @return
     */
    List<MarketingPO> selectMarketingByIds(List<Integer> idList);
}
