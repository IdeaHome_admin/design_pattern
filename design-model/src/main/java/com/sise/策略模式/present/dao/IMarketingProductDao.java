package com.sise.策略模式.present.dao;

import com.sise.策略模式.present.model.MarketingProductPO;

import java.util.List;

/**
 * @Author idea
 * @Date created in 3:44 下午 2020/5/4
 */
public interface IMarketingProductDao {

    /**
     * 按照产品编码查询
     *
     * @param productNo 产品编码
     * @return
     */
    List<MarketingProductPO> selectByProductNo(String productNo);
}
