package com.sise.策略模式.present.dao.impl;

import com.sise.策略模式.present.dao.IMarketingProductDao;
import com.sise.策略模式.present.model.MarketingProductPO;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author idea
 * @Date created in 3:45 下午 2020/5/4
 */
@Repository
public class MarketingProductDao implements IMarketingProductDao {

    private static List<MarketingProductPO> MARKET_PRODUCT_LIST = new ArrayList<>();

    static {
        MarketingProductPO marketingProductPO = MarketingProductPO.builder()
                .productNo("p111")
                .marketingId(2)
                .validStatus(1)
                .des("2999套餐-发放优惠券")
                .build();
        MarketingProductPO marketingProductPO2 = MarketingProductPO.builder()
                .productNo("p111")
                .marketingId(3)
                .validStatus(1)
                .des("2999套餐-满额红包返现")
                .build();

        MARKET_PRODUCT_LIST.add(marketingProductPO);
        MARKET_PRODUCT_LIST.add(marketingProductPO2);
    }

    @Override
    public List<MarketingProductPO> selectByProductNo(String productNo) {
        List<MarketingProductPO> marketingProductPOS = new ArrayList<>();
        for (MarketingProductPO marketingProductPO : MARKET_PRODUCT_LIST) {
            //产品编码一致 而且规则有效
            if(marketingProductPO.getProductNo().equals(productNo) && marketingProductPO.getValidStatus()==1){
                marketingProductPOS.add(marketingProductPO);
            }
        }
        return marketingProductPOS;
    }
}
