package com.sise.策略模式.present.impl;

        import com.sise.策略模式.present.IMarketingStrategy;
        import org.springframework.stereotype.Service;

/**
 * 折扣优惠
 *
 * @Author idea
 * @Date created in 9:30 上午 2020/5/4
 */
@Service
public class DiscountStrategy implements IMarketingStrategy {


    @Override
    public boolean doMarketing(Object... param) {
        System.out.println("打折优惠");
        return false;
    }
}
