package com.sise.策略模式.present.impl;

import com.sise.策略模式.present.IMarketingCoreService;
import com.sise.策略模式.present.IMarketingStrategy;
import com.sise.策略模式.present.dao.IMarketingDao;
import com.sise.策略模式.present.dao.IMarketingProductDao;
import com.sise.策略模式.present.model.MarketingPO;
import com.sise.策略模式.present.model.MarketingProductPO;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 营销工具核心执行器
 *
 * @Author idea
 * @Date created in 9:34 上午 2020/5/4
 */
@Service
public class MarketingCoreService implements IMarketingCoreService {

    @Resource
    private IMarketingDao iMarketingDao;

    @Resource
    private IMarketingProductDao iMarketingProductDao;

    @Resource
    private ApplicationContext applicationContext;

    @Override
    public boolean doMarketingJob(String productNo) throws ClassNotFoundException {
        System.out.println("doMarketingJob begin =============");
        System.out.println(productNo);
        List<MarketingProductPO> marketingProductPOS = iMarketingProductDao.selectByProductNo(productNo);
        if (marketingProductPOS != null) {
            List<Integer> marketingIdList = marketingProductPOS.stream().map(MarketingProductPO::getMarketingId).collect(Collectors.toList());
            List<MarketingPO> marketingPOS = iMarketingDao.selectMarketingByIds(marketingIdList);
            for (MarketingPO marketingPO : marketingPOS) {
                String marketingName = marketingPO.getMarketingName();
                Class<?> clazz = Class.forName(marketingName);
                IMarketingStrategy marketingStrategy = (IMarketingStrategy) applicationContext.getBean(clazz);
                marketingStrategy.doMarketing(marketingPO.getInputVal());
            }
            System.out.println("doMarketingJob end =============");
            return true;
        }
        System.out.println("doMarketingJob setting is empty ===========");
        return false;
    }
}
