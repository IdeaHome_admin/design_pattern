package com.sise.策略模式.present.impl;

import com.sise.策略模式.present.IMarketingStrategy;
import org.springframework.stereotype.Service;

/**
 * 新人赠送优惠券的策略
 *
 * @Author idea
 * @Date created in 9:23 上午 2020/5/4
 */
@Service
public class NewMemberCouponStrategy implements IMarketingStrategy {


    @Override
    public boolean doMarketing(Object... param) {
        System.out.println("新人赠送策略");
        return false;
    }
}
