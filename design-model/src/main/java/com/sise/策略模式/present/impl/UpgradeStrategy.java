package com.sise.策略模式.present.impl;

import com.sise.策略模式.present.IMarketingStrategy;
import org.springframework.stereotype.Service;

/**
 * 会员升级策略
 *
 * @Author idea
 * @Date created in 9:29 上午 2020/5/4
 */
@Service
public class UpgradeStrategy implements IMarketingStrategy {

    @Override
    public boolean doMarketing(Object... param) {
        System.out.println("升级策略");
        return false;
    }
}
