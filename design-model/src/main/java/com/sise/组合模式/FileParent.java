package com.sise.组合模式;

//抽象文件接口
public abstract class FileParent {
    public String fileName;
    public abstract Files add(FileParent fileParent);
    public abstract void remove();
    public abstract void clear();
    public abstract FileParent getChild();
}
