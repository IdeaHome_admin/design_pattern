package com.sise.观察者模式;

/**
 * @Author idea
 * @Date created in 9:00 上午 2020/7/11
 */

public interface ApplicationListener {

    /**
     * 绑定一个事件
     *
     * @param eventObject
     */
    void bind(EventObject eventObject);
}
