package com.sise.观察者模式;

import com.sise.观察者模式.constants.EventTypeEnum;

/**
 * 事件源头 因为消息的发送在接收端是需要根据参数判断的，所以一般都会封装一个source字段
 *
 * @Author idea
 * @Date created in 10:25 上午 2020/7/11
 */
public class EventObject<T> {

    private long time;

    private EventTypeEnum typeEnum;

    private T source;

    public EventObject(T object, EventTypeEnum eventTypeEnum, long time) {
        this.source = object;
        this.typeEnum = eventTypeEnum;
        this.time = time;
    }

    public EventObject(T object, EventTypeEnum eventTypeEnum) {
        this.time = System.currentTimeMillis();
        this.typeEnum = eventTypeEnum;
        this.source = object;
    }

    public T getSource() {
        return source;
    }

    public EventTypeEnum getTypeEnum(){
        return typeEnum;
    }

}
