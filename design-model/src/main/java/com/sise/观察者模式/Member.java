package com.sise.观察者模式;


import com.sise.观察者模式.constants.EventTypeEnum;
import com.sise.观察者模式.model.MsgObj;

/**
 * @Author idea
 * @Date created in 10:23 上午 2020/7/11
 */
public class Member {

    private int id;
    private String name;
    private boolean isVip;

    public Member(String name) {
        this.name = name;
    }

    public void upgrade(ApplicationListener applicationListener) {
        MsgObj msgObj = new MsgObj(name+"会员充钱了，升级");
        EventObject eventObject = new EventObject(msgObj, EventTypeEnum.UPGRADE_TYPE);
        applicationListener.bind(eventObject);
    }

    public void degrade(ApplicationListener applicationListener) {
        MsgObj msgObj = new MsgObj(name+"会员退费了，降级");
        EventObject eventObject = new EventObject(msgObj, EventTypeEnum.DEGRADE_TYPE);
        applicationListener.bind(eventObject);
    }


}
