package com.sise.观察者模式;

import com.sise.观察者模式.listener.UpdateListener;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Author idea
 * @Date created in 10:52 上午 2020/7/11
 */
public class Test {


    public static void main(String[] args) {
        ApplicationListener applicationListener = new UpdateListener();
        Member member = new Member("idea∂");
        member.degrade(applicationListener);
        member.upgrade(applicationListener);
    }
}
