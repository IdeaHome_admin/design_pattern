package com.sise.观察者模式.constants;

/**
 * @Author idea
 * @Date created in 10:32 上午 2020/7/11
 */
public enum EventTypeEnum {

    UPGRADE_TYPE(1, "升级监听器"),
    DEGRADE_TYPE(2,"降级事件");
    EventTypeEnum(int code, String des) {
        this.code = code;
        this.des = des;
    }

    int code;
    String des;


}
