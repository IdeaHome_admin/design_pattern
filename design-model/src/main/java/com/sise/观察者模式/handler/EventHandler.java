package com.sise.观察者模式.handler;

import com.sise.观察者模式.ApplicationListener;
import com.sise.观察者模式.EventObject;


/**
 * @Author idea
 * @Date created in 10:47 上午 2020/7/11
 */
public class EventHandler implements Runnable{
    private ApplicationListener applicationListener;
    private volatile EventObject eventObject;

    public EventHandler(ApplicationListener applicationListener, EventObject eventObject){
        this.applicationListener=applicationListener;
        this.eventObject=eventObject;
    }

    @Override
    public void run() {
        applicationListener.bind(eventObject);
    }
}
