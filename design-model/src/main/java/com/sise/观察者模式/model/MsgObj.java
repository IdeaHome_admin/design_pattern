package com.sise.观察者模式.model;

/**
 * @Author idea
 * @Date created in 10:39 上午 2020/7/11
 */
public class MsgObj {
    String msg;

    public MsgObj(String msg) {
        this.msg = msg;
    }

    public void print() {
        System.out.println("========" + msg + "=======");
    }
}
