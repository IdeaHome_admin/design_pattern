package com.sise.设计原则.依赖反转原则;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author linhao
 * @Date created in 10:44 上午 2021/9/5
 */
public class BeanContext {

    private static List<BeanObject> beanObjectList = new ArrayList<>();

    static {
        beanObjectList.add(new MessageNotify());
    }

    public static void main(String[] args) {
        beanObjectList.get(0).run();
    }
}
