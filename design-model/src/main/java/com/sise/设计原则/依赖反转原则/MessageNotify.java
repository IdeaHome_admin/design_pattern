package com.sise.设计原则.依赖反转原则;

/**
 * @Author linhao
 * @Date created in 10:45 上午 2021/9/5
 */
public class MessageNotify implements BeanObject{

    @Override
    public void run() {
        System.out.println("消息发送");
    }
}
