package com.sise.设计原则.单一责任原则;

import com.sise.设计概述.Address;

/**
 * @Author linhao
 * @Date created in 7:22 上午 2021/9/3
 */
public class UserInfo {

    private String username;
    private short age;
    private short height;
    private String phone;
    private String home;

    /**
     * 地址对象
     */
    private Address address;

    /**
     * 省份
     */
    private String province;
    /**
     * 城市
     */
    private String city;
    /**
     * 地区
     */
    private String region;
    /**
     * 街道
     */
    private String street;
}
