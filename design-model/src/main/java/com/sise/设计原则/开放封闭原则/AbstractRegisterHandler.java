package com.sise.设计原则.开放封闭原则;

/**
 * @Author linhao
 * @Date created in 8:10 上午 2021/9/3
 */
public abstract class AbstractRegisterHandler {

    /**
     * 获取注册渠道ID
     *
     * @return
     */
    public abstract int getSource();

    /**
     * 注册之后的核心通知模块程序
     *
     * @param registerInputParam
     * @return
     */
    public abstract boolean doPostProcessorAfterRegister(RegisterInputParam registerInputParam);

}
