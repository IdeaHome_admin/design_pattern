package com.sise.设计原则.开放封闭原则;

/**
 * @Author linhao
 * @Date created in 8:17 上午 2021/9/3
 */
public class RegisterConstants {

    public enum RegisterEnum{

        GZH_CHANNEL(0,"公众号渠道"),
        APP_CHANNEL(1,"app渠道");

        RegisterEnum(int code, String desc) {
            this.code = code;
            this.desc = desc;
        }

        int code;
        String desc;

        public int getCode() {
            return code;
        }
    }
}
