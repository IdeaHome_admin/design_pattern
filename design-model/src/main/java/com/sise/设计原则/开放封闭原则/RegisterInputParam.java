package com.sise.设计原则.开放封闭原则;

/**
 * @Author linhao
 * @Date created in 8:07 上午 2021/9/3
 */
public class RegisterInputParam {

    private long userId;

    private int source;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }
}
