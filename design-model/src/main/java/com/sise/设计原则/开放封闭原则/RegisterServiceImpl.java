package com.sise.设计原则.开放封闭原则;

import com.sise.设计原则.开放封闭原则.handler.AppRegisterHandler;
import com.sise.设计原则.开放封闭原则.handler.GZHRegisterHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author linhao
 * @Date created in 7:48 上午 2021/9/3
 */
public class RegisterServiceImpl implements IRegisterService {

    private static List<AbstractRegisterHandler> registerHandlerList = new ArrayList<>();

    static {
        registerHandlerList.add(new GZHRegisterHandler());
        registerHandlerList.add(new AppRegisterHandler());
    }

    @Override
    public void postProcessorAfterRegister(RegisterInputParam registerInputParam) {
        for (AbstractRegisterHandler abstractRegisterHandler : registerHandlerList) {
            if(abstractRegisterHandler.getSource()==registerInputParam.getSource()){
                abstractRegisterHandler.doPostProcessorAfterRegister(registerInputParam);
                return;
            }
        }
        throw new RuntimeException("未知注册渠道号");
    }

}
