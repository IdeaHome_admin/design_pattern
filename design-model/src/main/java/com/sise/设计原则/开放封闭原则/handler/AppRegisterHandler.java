package com.sise.设计原则.开放封闭原则.handler;

import com.sise.设计原则.开放封闭原则.AbstractRegisterHandler;
import com.sise.设计原则.开放封闭原则.RegisterConstants;
import com.sise.设计原则.开放封闭原则.RegisterInputParam;

/**
 * @Author linhao
 * @Date created in 8:16 上午 2021/9/3
 */
public class AppRegisterHandler extends AbstractRegisterHandler {

    @Override
    public int getSource() {
        return RegisterConstants.RegisterEnum.APP_CHANNEL.getCode();
    }

    @Override
    public boolean doPostProcessorAfterRegister(RegisterInputParam registerInputParam) {
        System.out.println("app处理逻辑");
        return true;
    }
}
