package com.sise.设计原则.开放封闭原则.handler;

import com.sise.设计原则.开放封闭原则.AbstractRegisterHandler;
import com.sise.设计原则.开放封闭原则.RegisterConstants;
import com.sise.设计原则.开放封闭原则.RegisterInputParam;

/**
 * @Author linhao
 * @Date created in 8:16 上午 2021/9/3
 */
public class GZHRegisterHandler  extends AbstractRegisterHandler {

    @Override
    public int getSource() {
        return RegisterConstants.RegisterEnum.GZH_CHANNEL.getCode();
    }

    @Override
    public boolean doPostProcessorAfterRegister(RegisterInputParam registerInputParam) {
        System.out.println("公众号处理逻辑");
        return true;
    }
}
