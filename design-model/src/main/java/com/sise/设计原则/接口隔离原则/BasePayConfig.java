package com.sise.设计原则.接口隔离原则;

import java.util.Map;

/**
 * 基本的支付配置接口
 *
 * @Author linhao
 * @Date created in 9:59 上午 2021/9/5
 */
public interface BasePayConfig {


    /**
     * 展示配置
     */
    Map<String,Object> showConfig(int code);

    /**
     * 更新配置信息
     *
     * @return
     */
    Map<String,Object> updateConfig();
}
