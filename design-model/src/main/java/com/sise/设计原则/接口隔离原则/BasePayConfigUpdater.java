package com.sise.设计原则.接口隔离原则;

import java.util.Map;

/**
 * @Author linhao
 * @Date created in 10:18 上午 2021/9/5
 */
public interface BasePayConfigUpdater {

    /**
     * 更新配置信息
     *
     * @return
     */
    Map<String,Object> updateConfig();
}
