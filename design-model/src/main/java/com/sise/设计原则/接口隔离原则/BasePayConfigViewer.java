package com.sise.设计原则.接口隔离原则;

import java.util.Map;

/**
 * @Author linhao
 * @Date created in 10:19 上午 2021/9/5
 */
public interface BasePayConfigViewer {

    /**
     * 展示配置
     */
    Map<String,Object> showConfig(int code);
}
