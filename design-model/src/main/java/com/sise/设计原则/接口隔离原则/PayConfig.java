package com.sise.设计原则.接口隔离原则;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author linhao
 * @Date created in 9:57 上午 2021/9/5
 */
public class PayConfig implements IPayConfig{

    public static Map<Integer,BasePayConfig> configList = new HashMap<>();

    static {
        //模拟
        configList.put(1,new AliPayConfig());
        configList.put(2,new BankPayConfig());
        configList.put(3,new WXPayConfig());
    }

    @Override
    public Map<String,Object> showConfig(int code) {
        BasePayConfig basePayConfig = configList.get(code);
        if(basePayConfig!=null){
            //模拟返回配置的信息
        }
        return new HashMap<>();
    }
}
