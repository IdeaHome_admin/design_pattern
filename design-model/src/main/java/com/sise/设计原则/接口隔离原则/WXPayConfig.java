package com.sise.设计原则.接口隔离原则;

import java.util.Map;

/**
 * @Author linhao
 * @Date created in 9:54 上午 2021/9/5
 */
public class WXPayConfig implements BasePayConfig{

    private String secretKey;

    private String appId;

    private String randomNumber;

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getRandomNumber() {
        return randomNumber;
    }

    public void setRandomNumber(String randomNumber) {
        this.randomNumber = randomNumber;
    }

    @Override
    public Map<String, Object> showConfig(int code) {
        return null;
    }

    @Override
    public Map<String, Object> updateConfig() {
        return null;
    }
}
