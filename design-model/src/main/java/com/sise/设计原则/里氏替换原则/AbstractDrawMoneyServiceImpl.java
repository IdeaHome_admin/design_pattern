package com.sise.设计原则.里氏替换原则;

/**
 * @Author linhao
 * @Date created in 11:25 上午 2021/9/4
 */
public abstract class AbstractDrawMoneyServiceImpl implements DrawMoneyService{

    /**
     * 设计初衷，如果扣款金额大于存款金额，则不允许提现
     *
     * @param drawMoneyInputParam
     */
    @Override
    public abstract void drawMoney(DrawMoneyInputParam drawMoneyInputParam);
}
