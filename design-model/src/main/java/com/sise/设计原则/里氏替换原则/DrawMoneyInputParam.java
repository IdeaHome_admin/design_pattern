package com.sise.设计原则.里氏替换原则;

import lombok.Data;

/**
 * @Author linhao
 * @Date created in 11:22 上午 2021/9/4
 */
@Data
public class DrawMoneyInputParam {

    private long userId;

    private Double money;
}
