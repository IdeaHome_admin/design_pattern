package com.sise.设计原则.里氏替换原则;

/**
 * @Author linhao
 * @Date created in 11:24 上午 2021/9/4
 */
public class WebDrawMoneyServiceImpl implements DrawMoneyService {

    @Override
    public void drawMoney(DrawMoneyInputParam drawMoneyInputParam) {
        System.out.println("web端提款业务");
    }
}
