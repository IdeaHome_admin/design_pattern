package com.sise.设计概述;

/**
 * 作者：idea
 * 日期：2018/3/23
 * 描述：地址
 */
public class Address {
    private String addressName;

    public Address() {
    }

    public Address(String addressName) {
        this.addressName = addressName;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }
}
