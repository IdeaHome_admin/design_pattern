package com.sise.设计概述;

/**
 * 作者：idea
 * 日期：2018/3/23
 * 描述：
 */
public class Driver {
    private String name; //司机的名字

    public Driver(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //设定可以开车去往东北（默认的地方）
    public void drive(Vehicle vehicle){
        System.out.println(this.getName()+"驾驶"+vehicle.getName());
        vehicle.go();
    }

    //设定可以开车去往某个地方
    public void drive(Vehicle vehicle,Address address){
        System.out.println(this.getName()+"驾驶"+vehicle.getName());
        vehicle.go(address);
    }

}
