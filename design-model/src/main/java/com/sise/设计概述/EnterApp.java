package com.sise.设计概述;

/**
 * 作者：idea
 * 日期：2018/3/23
 * 描述：函数入口
 */
public class EnterApp {
    public static void main(String[] args) {
        Driver driver=new Driver("老张");
        driver.drive(new Car("汽车"),new Address("北京"));
    }
}
