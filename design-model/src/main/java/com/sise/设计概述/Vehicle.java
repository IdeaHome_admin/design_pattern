package com.sise.设计概述;

/**
 * 作者：idea
 * 日期：2018/3/23
 * 描述：交通工具
 */
public class Vehicle {
    private String name;
    public Vehicle(String name){
        this.name=name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void go(){
        Address address=new Address("东北");
        System.out.println("去往"+address.getAddressName());
    }

    public void go(Address address){
        System.out.println("去往"+address.getAddressName());
    }

}
