package com.sise.责任链模式;

import java.util.ArrayList;
import java.util.List;

/**
 * @author：idea
 * @date：2018/2/3
 * @version： <h1></h1>
 */
public class FiliterChain implements Filiter {
    List<Filiter> filterList=new ArrayList<Filiter>();

    public FiliterChain addFiliter(Filiter filter){
        filterList.add(filter);
        return this;
    }


    @Override
    public String doFiliter(String str) {
        for (Filiter filiter : filterList) {
            str=filiter.doFiliter(str);
        }
        return str;
    }
}
