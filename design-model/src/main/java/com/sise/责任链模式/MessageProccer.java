package com.sise.责任链模式;

/**
 * @author：idea
 * @date：2018/2/3
 * @version： <h1></h1>
 */
//文本过滤功能类
public class MessageProccer {
    private String message;
    private Filiter filiter[]={ new HtmlFiliter(),new FaceFiliter()};

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String process(){
        for(Filiter filiter:filiter){
            message=filiter.doFiliter(message);
        }
        return message;
    }

}
