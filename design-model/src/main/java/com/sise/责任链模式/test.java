package com.sise.责任链模式;

/**
 * @author：idea
 * @date：2018/2/3
 * @version： <h1></h1>
 */
public class test {

    public FiliterChain filiterChain=new FiliterChain();

    public static void main(String[] args) {
        String str="<html></html> this is a test! :) ";
        FiliterChain filiterChain=new FiliterChain();
        filiterChain.addFiliter(new HtmlFiliter())
                .addFiliter(new FaceFiliter());
        String r=filiterChain.doFiliter(str);
        System.out.println(r);
    }
}
